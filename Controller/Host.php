<?php
class Controller_Host {
	private $model;
	private $view;

	public function __construct(){
		$this->model = new Model_Host();
		$this->view = new View_Host_ViewAll($this->model);
	}

	/**
	 * function deletes a host 
	 * hostname delivered via post
	 */
	public function delete(){
		if(isset($_POST['hostname'])){
			Model_Host::deleteByName($_POST['hostname']);
		}
		$this->view->render();
	}

	/**
	 * Function to create a new model
	 * renders the view all View
	 */
	public function create(){
		if(isset($_POST['hostname'])){
			Model_Host::insert($_POST['hostname']);
		}
		$this->view->render();
	}

	/**
	 * Function to validate posible hostname from DB via AJAX (no duplicates names allowed)
	 */
	public function ajaxValidateName(){
		if( isset($_POST['hostname'])) {
			if(Model_Host::exists($_POST['hostname'])){
				$validation = true;
			}	
			else{
				$validation = false;
			}
			$ret = array('validation'=>$validation);
			echo json_encode($ret);
		}
	}



}