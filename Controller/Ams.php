<?php
class Controller_Ams {
	private $model;
	private $view;

	public function __construct(){
		$this->model = new Model_Ams();
		$this->view = new View_Ams_View($this->model);
	}

	/**
	 * Function to create an AMS 
	 */
	public function create(){
		$amsName = strip_tags($_POST['name']);
		$userName = $_SESSION['username'];
		$status = 'created';

		$this->model->init($amsName,null,array(),$userName,$status);
		// foreach ($_POST['hostServices'] as $elm){
		$hostService = json_decode($_POST['hostServices'][0],true);
		for($i = 0; $i < count($hostService); $i++){
			$host = Model_Host::getByName($hostService[$i]['host']);
			$service = Model_Service::getByName($hostService[$i]['service']);
			
			$this->model->addHostService($host,$service);
		}
		if($this->model->validate()){
			$this->model->insert();
		}
		$this->view->render();
	}

	/**
	 * Delete an AMS by 
	 */
	public function delete(){
		Model_Ams::deleteById($_POST['amsname']);
		$this->view->render();
	}
}