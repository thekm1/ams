<?php
class Controller_User {
	private $model;
	private $view;

	public function __construct(){
		$this->model = new Model_User();
	}

	/**
	 * function deletes a user 
	 * username delivered via post
	 */
	public function delete(){
		if(isset($_POST['username'])){
			Model_User::deleteByName($_POST['username']);
		}
	}

	public function loginUser(){
		if ($this->model->checklogin($_POST['username'],$_POST['password'])){
			$_SESSION["username"] = $_POST['username'];
			$newView = new View_User_ViewLogin($this->model);
			header("location: http://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'].add_param( $_SERVER['PHP_SELF'], "route", 'user'),  true,  301 );  exit;
		}
		else {
			$newView = new View_User_ViewLogin($this->model);
			$newView->render("Login was not succeccfull");
		}
	}

	public function renderRegistration(){
		$newView = new View_User_ViewRegistration($this->model);
		$newView->render();
	}

	public function logoutUser(){
		$_SESSION=[];
		setcookie(session_name(),'',1);
		$newView = new View_User_ViewLogin($this->model);
		header("location: http://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'].add_param( $_SERVER['PHP_SELF'], "route", 'user'),  true,  301 );  exit;
	}

	public function updatePassword(){
		$success=true;
		$errorarray = array();
		if (!($_POST['password'] === $_POST['password2'])) {
			$success=false;
			$errorarray += array("<p>password is not equal</p>");
		}

		if ($success) {
			$array = array(
				"username" => $_POST['username'],
				"password" => password_hash(strip_tags($_POST['password']),PASSWORD_BCRYPT),
			);	
			Model_User::updatePassword($array);
		}
		
		$newView = new View_User_ViewLogin($this->model);
		$newView->renderLoginSuccessfull();

	}

	public function create(){
		$errorarray = array();
		$success=true;
		if (empty($_POST['firstname'])) {
			$success=false;
			$errorarray += array("<p>first name is empty</p>");
		}
		if (empty($_POST['lastname'])) {
			$success=false;
			$errorarray += array("<p>lastname name is empty</p>");
		}

		if (empty($_POST['username'])) {
			$success=false;
			$errorarray += array("<p>username name is empty</p>");
		}
		
		if (!$_POST['acceptagb']) {
			$success=false;
			$errorarray += array("<p>you have to accept AGBs</p>");
		}
		
		if (empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
				$success=false;
				$errorarray += array("<p>email has wrong format</p>");
		}
		if ($this->model->exists($_POST['username'])){
			$success=false;
			$errorarray += array("<p>User already exists</p>");
		}

		if (!($_POST['password'] === $_POST['password2'])) {
			$success=false;
			$errorarray += array("<p>password is not equal</p>");
		}
		if (!empty($_FILES["image"])) {
			$maxsize    = 2097152;
			$acceptable = array(
				'image/jpeg',
				'image/jpg',
				'image/gif',
				'image/png'
			);

			if(($_FILES['picture']['size'] >= $maxsize) || ($_FILES["picture"]["size"] == 0)) {
				$success=false;
				$errorarray += array("<p>File to large</p>");
			}

			if((!in_array($_FILES['picture']['type'], $acceptable)) && (!empty($_FILES["picture"]["type"]))) {
				$success=false;
				$errorarray += array("<p>Pciture type not accepted</p>");
			}
		}

		if ($success) {
			if (!empty($_FILES["image"])) {
				$file = $_FILES['picture'];		
				move_uploaded_file($file['tmp_name'],$_SERVER['DOCUMENT_ROOT']."/AMS/img/".$_POST['username']);
			}
			$array = array(
				"firstname" => strip_tags($_POST['firstname']),
				"lastname" => strip_tags($_POST['lastname']),
				"username" => strip_tags($_POST['username']),
				"email" => strip_tags($_POST['email']),
				"password" => password_hash(strip_tags($_POST['password']),PASSWORD_BCRYPT),
				"picture" => $_FILES['picture'],
			);
			Model_User::insert($array);
			mail($array['email'], 'Activation Mail', "You are activated", "From: webmaster@realstuff.ch");	
		}
		
		$newView = new View_User_ViewRegistrationSuccessfull();
		$newView->render($success, $errorarray);		
	}

	public function makeAdmin(){
		$this->model->makeAdmin($_POST['username']);
		$newView = new View_Role_ViewAll();
		$newView->render();
		header("location: http://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'].add_param( $_SERVER['PHP_SELF'], "route", 'role'),  true,  301 );  exit;
	}
	
	public function makeUser(){
		$this->model->makeUser($_POST['username']);
		$newView = new View_Role_ViewAll();
		$newView->render();
		header("location: http://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'].add_param( $_SERVER['PHP_SELF'], "route", 'role'),  true,  301 );  exit;
    }
	
	public static function isAdmin(){
		if(isset($_SESSION['username'])){
			return Model_User::getUserByUsername($_SESSION['username'])['isadmin'];
		}
		return false;
	}

	public static function userInfo(){
		echo "<span>";
		if(isset($_SESSION['username'])){
			if(Controller_User::isAdmin()){
				echo "Logged in as admin: ".$_SESSION['username'];
			}
			else{
				echo "Logged in as user: ".$_SESSION['username'] ;
			}
		}
		else{
			echo "You are currently not logged in" ;
		}
		echo "</span>";
	}


}