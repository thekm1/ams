<?php
class Controller_Service {
	private $model;
	private $view;

	public function __construct(){
		$this->model = new Model_Service();
		$this->view = new View_Service_ViewAll($this->model);
	}

	/**
	 * function deletes a host 
	 * hostname delivered via post
	 */
	public function delete(){
		if(isset($_POST['servicename'])){
			Model_Service::deleteByName($_POST['servicename']);
		}
		$this->view->render();
	}

	/**
	 * Function to create a new model
	 * renders the view all View
	 */
	public function create(){
		if(isset($_POST['servicename'])){
			Model_Service::insert($_POST['servicename']);
		}
		$this->view->render();
	}

	/**
	 * Function to validate posible servicename from DB via AJAX (no duplicates names allowed)
	 */
	public function ajaxValidateName(){
		if( isset($_POST['servicename'])) {
			if(Model_Service::exists($_POST['servicename'])){
				$validation = true;
			}	
			else{
				$validation = false;
			}
			$ret = array('validation'=>$validation);
			echo json_encode($ret);
		}
	}



}