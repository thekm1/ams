<?php 
class DB extends mysqli{
	// const HOST="localhost", USER="zuserf21_ams_user", PW="ams_password", DB_NAME="zuserf21_AMS";
	const HOST="localhost", USER="prog", PW="prog", DB_NAME="data";
	static private $instance;

	function __construct() {
		parent::__construct(self::HOST,self::USER, self::PW, self::DB_NAME);
	}

	static public function getInstance() {
		if(!self::$instance) {
			@self::$instance = new DB();
			if(self::$instance->connect_errno > 0){
				die("Unable to connect to database, causse of error [".self::$instance->connect_error."]");
			}
			if(!self::$instance->set_charset("utf8")) {
				die("Error laoding character set utf8: ".self::$instance->error);
			}
		}
		return self::$instance;
	}

	static public function doQuery($sql) {
		return self::getInstance()->query($sql);
	}
}