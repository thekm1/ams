<?php

function get_param($name, $default) {
  if (!isset($_GET[$name])){
    return $default;
  }
  return urldecode($_GET[$name]);
}
  
function add_param($url, $name, $value) {
  if(strpos($url, '?') !== false) {
    $sep = '&';
  }
  else {
    $sep = '?';
  }
  return $url . $sep . $name . "=" . urlencode($value);
}

/**
 * return the actual set language, session for language for user not set take delivered default value
 */
function getLanguage($default) {
  if(isset($_GET['lang'])) {
    $_SESSION['lang'] = $_GET['lang'];
  }
  if(!isset($_SESSION['lang'])){
    $_SESSION['lang'] = $default;
  }
  setcookie('lang',$_SESSION['lang'],time()+60*60*24);
  return $_SESSION['lang'];
}

function navigation($pageId) {
  if (isset($_SESSION['username']) && Controller_User::isAdmin()){
    $pages = array(
      '0'=>'user',
      '1'=>'role',
      '2'=>'host',
      '3'=>'service',
      '4'=>'ams',
    );
  }
  elseif(isset($_SESSION['username'])) {
    $pages = array(
      '0'=>'user',
      '1'=>'ams',
    );
  }
  else{
    $pages = array(
      '0'=>'user',
    );
  }
  for ($i = 0; $i < count($pages); $i++) {
    $url = add_param( $_SERVER['PHP_SELF'], "route", $pages[$i]);
    $class = $pageId == $pages[$i] ? 'nav-link active' : 'nav-link';
    echo "<a class='$class' href='$url'>".t($pages[$i])."</a>";
  }
}

function languages($language, $pageId) {
  $languages = ['de','en'];
  $urlbase = add_param($_SERVER['PHP_SELF'], 'route', $pageId);
  foreach( $languages as $l) {
    $class = $language == $l ? 'active bg-primary' : 'inactive';
    echo "<a class='col text-right ".$class."' href=";
    echo add_param($urlbase,'lang', $l).">".t($l)."</a>";
  }
}

function t($key) {
  $texts = array(
  'de' => array(
    'de'=>'De',
    'en'=>'De'),
  'en' => array(
    'de'=>'En',
    'en'=>'En'),
  'de' => array(
    'de'=>'De',
    'en'=>'De'),
  'page' => array(
    'de'=>'Seite',
    'en'=>'Page'),
  'home' => array(
    'de'=>'Hauptseite',
    'en'=>'Home'),
  'user' => array(
    'de'=>'Benutzer',
    'en'=>'User'),
  'role' => array(
    'de'=>'Rolle',
    'en'=>'Role'),
  'host' => array(
    'de'=>'Server',
    'en'=>'Hosts'),
  'service' => array(
    'de'=>'Dienste',
    'en'=>'Service'),
  'ams' => array(
    'de'=>'AMS',
    'en'=>'AMS'),
  'view/registration' => array(
    'de'=>'Ansicht und Registrierung',
    'en'=>'view and register'),
  'name' => array(
    'de'=>'Name',
    'en'=>'Name'),
  'content' => array(
    'de'=>'Willkommen auf der Seite ',
    'en'=>'Welcome to the page '),
  'del' => array(
    'de'=>'Löschen',
    'en'=>'Delete'),
  'create' => array(
    'de'=>'Erstellen',
    'en'=>'Create'),
  'hometext1' => array(
    'de'=>'das ist text nummer eins',
    'en'=>'this is the text number one'),
  'hometext2' => array(
      'de'=>'das ist text nummer zwei',
      'en'=>'this is the text number two'),
  'login_name' => array(
        'de'=>'Benutzer',
        'en'=>'User'),
  'login_password' => array(
          'de'=>'Passwort',
          'en'=>'password'),
  );
  return $texts[$key][$_SESSION['lang']] ?? "[$key]";
}

function goToLogin(){
  echo "<p>You are not logged in</p>";
  echo '<p><a href="index.php?route=user">Please log in first</a>.</p>';
}
