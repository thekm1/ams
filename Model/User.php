<?php
class Model_User{
    private $firstname;
    private $lastname;
    private $username;
    private $email;
    private $picture;
    private $password;
    private $creationTimeStamp; 
    private $isadmin; 

   static public function getUsers(){
       $users = array();
       $res = DB::doQuery (
           "SELECT * FROM user;"
       );
       if (!$res) return null;
       while ($user = $res->fetch_object(get_class())){
        $users[] = $user;
       
    }
       return $users;
   }
   public function getUser($username){
    $res = DB::doQuery(
        "SELECT * FROM user WHERE username = $username;"
    );
    if (!$res) return null;
    return $res->fetch_object(get_class());
    }

   static public function insert($values) {
       $stmt=DB::getInstance()->prepare(
        "INSERT INTO user ".
        "(firstname, lastname, username, email, password, picture) ".
        "VALUE (?,?,?,?,?,?)"
       );
       $success=$stmt->bind_param(
           'sssssb', 
           $values['firstname'],
           $values['lastname'],
           $values['username'],
           $values['email'],
           $values['password'],
           $values['picture']
        );
       return $stmt->execute();
   }

   static public function updatePassword($values) {
    $stmt=DB::getInstance()->prepare(
     "UPDATE user SET password = ? WHERE username = ?"
    );
    $success=$stmt->bind_param(
        'ss', 
        $values['password'],
        $values['username']
     );
     return $stmt->execute(); 
    }

   static public function exists($val){
       $stmt=DB::getInstance()->prepare(
        "SELECT 1 FROM user WHERE (username=?)"
       );
       $success = $stmt->bind_param('s',$val);
       $stmt->execute();
       $stmt->store_result();
       return $stmt->num_rows > 0 ? true : false;

}

   static public function getUserByUsername($username) {
    $stmt=DB::getInstance()->prepare(
        "SELECT * FROM user WHERE username=?"
     );
     $stmt->bind_param('s',$username);
     $stmt->execute();
     $result=$stmt->get_result();
     if (!$result||$result->num_rows!==1) return false;
     $row = $result->fetch_assoc();
     return $row;
   }

   public function save(){
       $sql = sprintf (
           "UPDATE user
           set firstname='%s', lastname='%s', username='%s', email='%s', picture='%s', password='%s' 
           WHERE username = %s;",
           $this->firstname, $this->lastname, $this->username, $this->email, 
           $this->picture, $this->password
       );
       $res = DB::doQuery($sql);
       return $res != null;

   }

   public function checklogin($login,$password){ 
       $stmt=DB::getInstance()->prepare(
           "SELECT * FROM user WHERE username=?"
        );
        $stmt->bind_param('s',$login);
        $stmt->execute();
        $result=$stmt->get_result();
        if (!$result||$result->num_rows!==1) return false;
        $row = $result->fetch_assoc();
        return password_verify($password,$row["password"]);
    }

   public function getUserName(){
       return $this->username;
   }

   public function getFirstName(){
    return $this->firstname;
    }
    public function getLastName(){
    return $this->lastname;
    }
    public function isAdmin(){
        return $this->isadmin;
    }

    public function makeAdmin($username){
        $temp = 1;
        $stmt=DB::getInstance()->prepare(
            "UPDATE user SET isadmin = ? WHERE username = ?"
           );
        $success=$stmt->bind_param(
               'is', 
               $temp,
               $username
            );
        return $stmt->execute(); 
    }

    public function makeUser($username){
        $temp = 0;
        $stmt=DB::getInstance()->prepare(
            "UPDATE user SET isadmin = ? WHERE username = ?"
           );
        $success=$stmt->bind_param(
               'is', 
               $temp,
               $username
            );
        return $stmt->execute(); 
    }

}
?>