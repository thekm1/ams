	<?php 
	class Model_Host{
		private $name;
		private $oldname;

	public function getName(){
		return $this->name;
	}

	/**
	 * Set the name of this HostModel
	 */
	public function setName($val){
		$this->name = DB::getInstance()->escape_string(strip_tags($val));
	}

	/**
	 * Saves this host intp DB data into table host
	 */
	public function save() {
		if(!$this->validate()){
			echo "cannot be saved, the name is null";
			return false;
		}
		$sql = sprintf("UPDATE host SET name='%s' WHERE name='%s'",$this->name,$this->oldname);
		$res = DB::doQuery($sql);
		if(!$res){
			echo "null";
		}
		elseif( $res === true){
			$this->oldname = $this->name;
			echo "true";
		}
		else{
			echo "false";
		}
		return $res != null;
	}

	/**
	 * check if the var name is null or empty
	 */
	private function validate(){
		return !is_null($this->name) && $this->name !== "";
	}

	/**
	 * Create a new entry into DB data into table host
	 */
	static public function insert($hostname){
		$newhost = new Model_Host();
		$newhost->setName($hostname);
		if(!$newhost->validate()){
			echo "cannot be saved, the name is null";
			return false;
		}
		if(self::exists($hostname)){
			echo DB::getInstance()->error;
			return false;
		}
		$stmt = DB::getInstance()->prepare("INSERT INTO host (name) VALUE(?)");
		if (!$stmt){
			die("Error during preparing statement in HostModel insert method: ".DB::getInstance()->error);
		}
		$bindParam = $newhost->getName();
		$success = $stmt->bind_param('s',$bindParam);
		if(!$success) {
			die("Error during binding param in HostModel insert method: ".DB::getInstance()->error);
		}
		return $stmt->execute();

	}
/**
 * returns the boolean condition if value an entry exists in DB with entered value as name
 */
	static public function exists($val){
		$stmt = DB::getInstance()->prepare("SELECT 1 FROM host WHERE (name=?)");
		if (!$stmt){
			die("Error during preparing statement in HostModel exists method :".DB::getInstance()->error);
		}
		$success = $stmt->bind_param('s',$val);
		if(!$success) {
			die("Error during binding param in HostModel exists method :".DB::getInstance()->error);
		}
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0 ? true : false;

	}

	static public function getByName($val){
		$sanVal = DB::getInstance()->escape_string($val);
		$res = DB::doQuery("SELECT * FROM `host` WHERE `name` LIKE '$sanVal'");
		if (!$res){
			return null;
		}
		$h = $res->fetch_object(get_class());
		if($h === false){
			echo "shit happend";
			return null;
		}
		$host = new Model_Host();
		$host->setName($h->name);
		return $host;
	}

	/**
	 * deletes the row from db
	 */
	static public function deleteByName($name) {
		if(!self::exists($name)){
			echo "nicht im DB vorhanden";
			return false;
		}
		$stmt = DB::getInstance()->prepare("DELETE FROM host WHERE name=?");
		if (!$stmt){
			die("Error during preparing statement in HostModel delete method: ".DB::getInstance()->error);
		}
		$success = $stmt->bind_param('s',$name);
		if(!$success) {
			die("Error during binding param in HostModel delete method: ".DB::getInstance()->error);
		}
		return $stmt->execute();
	}

	/**
	 * gets all entries from DB
	 */
	public static function getAllHosts(){
		$hosts = array();
		$res = DB::doQuery("SELECT * FROM host;");
		if (!$res){
			return null;
		}
		while($h = $res->fetch_assoc()) {
			$host = new Model_Host();
			$host->setName($h['name']);
			$hosts[] = $host;
		}
		return $hosts;
	}

}