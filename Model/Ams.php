<?php
class Model_Ams{
	private $name;
	private $id;
	private $hostServcies;
	private $user;
	private $status = 'created';

	public function init($name, $id=null, $hostServcies=array(), $user, $status='created'){
		$this->name = DB::getInstance()->escape_string(strip_tags($name));
		$this->id = (int)$id;
		$this->hostServcies = $hostServcies;
		$this->user = isset($user) ? $user: $_SESSION['user'];
		$this->status = $status;
	}

	public function addHostService(Model_Host $host, Model_Service $service){
		array_push($this->hostServcies,array('host'=>$host,'service'=>$service));
	}

	public function getName(){
		return $this->name;
	}

	public function getStatus(){
		return $this->status;
	}

	public function getId() {
		return $this->id;
	}

	public function getUser(){
		return $this->user;
	}

	public function getHostServices(){
		return $this->hostServcies;
	}

	public function validate(){
		$valid = true;
		if(!isset($this->name) || $this->name === ''){
			echo "Please set name of AMS. ";
			$valid = false;
		}
		if(!isset($this->user) || $this->user === ''){
			echo "Please login";
			$valid = false;
		}
		if(empty($this->hostServcies)){
			echo "Please select host and service";
			$valid = false;
		}
		return $valid;
	}

	static public function getAllAms(){
		$ams_arr= array();
		$res = DB::doQuery("SELECT * FROM ams");
		if (!$res){
			echo "problem0";
			return null;
		}
		while($val = $res->fetch_assoc()) {
			$res2 = DB::doQuery("SELECT * FROM ams_hostservice WHERE fk_idams=".$val['id'].";");
			if (!$res){
				echo "problem";
				return null;
			}
			$hostservice_arr = array();
			while($val2 = $res2->fetch_assoc()){
				$host = new Model_Host();
				$host->setName($val2['fk_host']);
				$service = new Model_Service();
				$service->setName($val2['fk_service']);
				$hostservice_arr[] = array('host'=>$host, 'service'=>$service);
			}
			$ams = new Model_Ams();
			$ams->init($val['name'],$val['id'],$hostservice_arr,$val['fk_username'],$val['status']);
			$ams_arr[] = $ams;
		}
		return $ams_arr;
	}

	static public function getMyAms(){
		$ams_arr= array();
		$res = DB::doQuery("SELECT * FROM `ams` WHERE `fk_username` LIKE '".$_SESSION['username']."'");
		if (!$res){
			echo "problem with query".DB::getInstance()->error;
			return null;
		}
		while($val = $res->fetch_assoc()) {
			$res2 = DB::doQuery("SELECT * FROM ams_hostservice WHERE fk_idams=".$val['id'].";");
			if (!$res){
				echo "problem with getting host and service";
				return null;
			}
			$hostservice_arr = array();
			while($val2 = $res2->fetch_assoc()){
				$host = new Model_Host();
				$host->setName($val2['fk_host']);
				$service = new Model_Service();
				$service->setName($val2['fk_service']);
				$hostservice_arr[] = array('host'=>$host, 'service'=>$service);
			}
			$ams = new Model_Ams();
			$ams->init($val['name'],$val['id'],$hostservice_arr,$val['fk_username'],$val['status']);
			$ams_arr[] = $ams;
		}
		return $ams_arr;
	}

	public function insert(){
		$stmt = DB::getInstance()->prepare("INSERT INTO ams (name,fk_username,status) VALUE(?,?,?)");
		if (!$stmt){
			die("Error during preparing statement in AMSModel insert method: ".DB::getInstance()->error);
		}
		$success = $stmt->bind_param('sss',$this->name,$this->user,$this->status);
		if(!$success) {
			die("Error during binding param in AMSModel insert method: ".DB::getInstance()->error);
		}
		if($stmt->execute()){
			$this->id = DB::getInstance()->insert_id;
			foreach($this->hostServcies as $hostservice){
				$stmt = DB::getInstance()->prepare("INSERT INTO ams_hostservice (fk_idams,fk_host,fk_service) VALUE(?,?,?)");
				if (!$stmt){
					die("Error during preparing statement in AMSModel insert method: ".DB::getInstance()->error);
				}
				$hostname = $hostservice['host']->getName();
				$servicename = $hostservice['service']->getName();
				$success = $stmt->bind_param('iss',$this->id,$hostname,$servicename);
				if(!$success) {
					die("Error during binding param in AMSModel insert method: ".DB::getInstance()->error);
				}
				if($stmt->execute()){
					continue;
				}
				else{
					return false;
				}
			}
			return true;
		}
	}

	static public function deleteById($id){
		$id = (int)$id;
		$res = DB::doQuery("DELETE FROM ams_hostservice WHERE fk_idams = $id");
		if($res){
			return  DB::doQuery("DELETE FROM ams WHERE id = $id");
		}
		else{
			return null;
		}
	}
}