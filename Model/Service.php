<?php 
class Model_Service{
	private $name;
	private $oldname;

	public function getName(){
		return $this->name;
	}

	/**
	 * Set the name of this ServiceModel
	 */
	public function setName($val){
		$this->name = DB::getInstance()->escape_string(strip_tags($val));
	}

	/**
	 * Saves this Service into DB data into table host
	 */
	public function save() {
		if(!$this->validate()){
			echo "cannot be saved, the name is null";
			return false;
		}
		$sql = sprintf("UPDATE service SET name='%s' WHERE name='%s'",$this->name,$this->oldname);
		$res = DB::doQuery($sql);
		if(!$res){
			echo "null";
		}
		elseif( $res === true){
			$this->oldname = $this->name;
			echo "true";
		}
		else{
			echo "false";
		}
		return $res != null;
	}

	/**
	 * check if the var name is null or empty
	 */
	private function validate(){
		return !is_null($this->name) && $this->name !== "";
	}

	/**
	 * Create a new entry into DB data into table host
	 */
	static public function insert($servicename){
		$newService = new Model_Service();
		$newService->setName($servicename);
		if(!$newService->validate()){
			echo "cannot be saved, the name is null";
			return false;
		}
		if(self::exists($newService->getName())){
			echo DB::getInstance()->error;
			return false;
		}
		$stmt = DB::getInstance()->prepare("INSERT INTO service (name) VALUE(?)");
		if (!$stmt){
			die("Error during preparing statement in ServiceModel insert method: ".DB::getInstance()->error);
		}
		$bindParam = $newService->getName();
		$success = $stmt->bind_param('s',$bindParam);
		if(!$success) {
			die("Error during binding param in ServiceModel insert method: ".DB::getInstance()->error);
		}
		return $stmt->execute();

	}
/**
 * returns the boolean condition if value an entry exists in DB with entered value as name
 */
	static public function exists($val){
		$stmt = DB::getInstance()->prepare("SELECT 1 FROM service WHERE (name=?)");
		if (!$stmt){
			die("Error during preparing statement in ServiceModel exists method :".DB::getInstance()->error);
		}
		$success = $stmt->bind_param('s',$val);
		if(!$success) {
			die("Error during binding param in ServiceModel exists method :".DB::getInstance()->error);
		}
		$stmt->execute();
		$stmt->store_result();
		return $stmt->num_rows > 0 ? true : false;
	}

	/**
	 * deletes the row from db
	 */
	static public function deleteByName($name) {
		if(!self::exists($name)){
			echo "was schief gelaufen";
			return false;
		}
		$stmt = DB::getInstance()->prepare("DELETE FROM service WHERE name=?");
		if (!$stmt){
			die("Error during preparing statement in HostModel delete method: ".DB::getInstance()->error);
		}
		$success = $stmt->bind_param('s',$name);
		if(!$success) {
			die("Error during binding param in HostModel delete method: ".DB::getInstance()->error);
		}
		return $stmt->execute();
	}

	/**
	 * gets all entries from DB
	 */
	public static function getAllServices(){
		$services = array();
		$res = DB::doQuery("SELECT * FROM service;");
		if (!$res){
			return null;
		}
		while($s = $res->fetch_assoc()) {
			$service = new Model_Service();
			$service->setName($s['name']);
			$services[] = $service;
		}
		return $services;
	}

	static public function getByName($val){
		$sanVal = DB::getInstance()->escape_string($val);
		$res = DB::doQuery("SELECT * FROM `service` WHERE `name` LIKE '$sanVal'");
		if (!$res){
			return null;
		}
		$s = $res->fetch_object(get_class());
		if($s === false){
			echo "shit happend";
			return null;
		}
		$service = new Model_Service();
		$service->setName($s->name);
		return $service;
	}

}