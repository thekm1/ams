  <script type="text/javascript" src="add_form_jsfunc.js"></script>

<div class ="form">
  <form action="/controller.php">
  <!--Name-->
  <div class="">
    <label for=""><?php echo t('name'); ?></label>
    <input type="text">
  </div>
  
  <!--Hosts -->
    <div class="inline left">
      <label for="">Hosts</label>
      <select name="hosts" id="hosts">
      <?php
        foreach(getHosts() as $host) {
          echo "<option>$host</option>";
        }
        ?>
      </select>
    </div>

    <!--Services -->
    <div class="inline left">
      <label for="">Servies</label>
      <select name="services" id="services">
      <?php 
        foreach(getServices() as $service) {
          echo "<option>$service</option>";
        }
        ?>
      </select>
    </div>

    <div class="inline left">
        <button type ="button" onclick="addSelection()">Add</button>
    </div>

    <div class="inline scroll">
        <label>Selected</label>
        <table class="table table-stripped" id="selectedMetrics">
          <thead>
            <tr>
              <th>Hosts</th>
              <th>Service</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        
    </div>
  </form>
</div>
