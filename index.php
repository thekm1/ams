<?php 
  session_start();
  include_once('functions.php');
  include_once('autoloader.php');
  //include_once('authentication.inc.php');
  $language = getLanguage('en');
  $pageId = get_param('route', 'login');
?>
<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8" content="initial-scale=1.0" name="viewport">
      <title>Application Monitoring System</title>
      <link rel="shortcut icon" href="img/amsLogo.png" type="favicon/ico" />
      <script  
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">
      </script>
      <link rel="stylesheet" href="mycss.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" 
        integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" 
          crossorigin="anonymous">
  </head>
  <body>
    <div id="header" class="container-fluid">
      <div class="row align-items-center">
        <div class="col-1 logo">
          <img id="ams-logo"class="img-fluid"src="img/amsLogo.png" alt="ams-logo" >
        </div>
        <!-- navigation  -->
        <div id="navigation-background " class="col-8 align-middle">
          <div class="nav nav-pills nav-justified">
            <?php navigation($pageId); ?>
          </div>
        </div>
        <!--Language selection -->
        <div class="language col-3">
          <div class="row align-right">
            <div class="col text-right">
              <?php Controller_User::userInfo(); ?>
            </div>
          </div>
          <div class="row align-right">
            <?php languages($language, $pageId); ?>
          </div>
        </div>
      </div>
    </div>

      <!-- My variable content -->
      <div class="container-fluid" id="content">
        <?php include_once('FrontController.php'); ?>
      </div>

      <div class="container-fluid" id="footer">
        <div class="row align-items-end">
          <div class="col">
            <img id="rs-logo" class="img-fluid"src="img/Logo_mit_Schweif.jpg" alt="logo" >
          </div>
          <div class="col text-right">
            <span>Webpage by Bruno Fernandez & Mathew Thekkekara</span>
          </div>
        </div>
      </div>
  </body>
</html>
