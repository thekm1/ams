<?php
class View_Error {
    
	public function __construct(){
	}

	static public function renderPageNotFound(){
        $path = str_replace(dirname(__FILE__,3),"",dirname(__FILE__,2));
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2">';
        echo "<img src='".$path."/img/page_not_found.gif' class='img-fluid' alt='Page not found'>";
        echo '</div>';
        echo '</div>';
    }
	static public function renderNoAccess(){
        $path = str_replace(dirname(__FILE__,3),"",dirname(__FILE__,2));
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2">';
        echo "<img src='".$path."/img/no_access.png' class='img-fluid' alt='Page not found'>";
        echo '</div>';
        echo '</div>';
    }
}