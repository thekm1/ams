<?php
class View_Host_ViewAll {
private $model;

	public function __construct(Model_Host $model){
		$this->model = $model;
	}

	public function render(){

		echo "<script type='text/javascript' src='View/Host/ajaxValidation.js'></script>";
		echo "<div class='row'><div class='col'>";
		echo "<table class='table table-sm table-hover'>";
		echo "<tr><th>Hostname</th><th>Actions</th></tr>";
		$hosts = Model_Host::getAllHosts();
		if(is_array($hosts)){
			foreach ($hosts as $host){
				echo "<tr>";
				echo "<td>".$host->getName()."</td>";
				echo "<form action='index.php?route=host&action=delete' method='post'>";
				echo "<input type='hidden' name='hostname' value='".$host->getName()."'>";
				echo "<td><input class='form-control btn btn-sm btn-danger'type='submit' value='Delete'/> "."</td>";
				echo "</form></tr>";
			}
		}
		echo "<tr>";
		echo "<form action='index.php?route=host&action=create' method='post'>";
		echo "<td> <input autofocus id='createInputName'class='form-control form-control-sm' type='text' name='hostname'> </td> ";
		echo "<td> <input class=' form-control btn btn-sm btn-primary'type='submit' value='".t('create')."'/>";
		echo "</tr>";
		echo "</table>";
		echo "</div></div>";
	}

}