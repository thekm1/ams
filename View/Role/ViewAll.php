<?php
class View_Role_ViewAll {
    
	public function __construct(){
	}

	public function render(){
        echo "<div class='row'><div class='col'>";
        echo "<table class='table table-sm table-hover'>";
        echo "<tr><th>Username</th><th>Is admin</th><th>Actions</th></tr>";
        foreach (Model_User::getUsers() as $user){
            echo "<tr>";
            echo "<td>".$user->getUserName()."</td>";
            echo "<td>".$user->isAdmin()."</td>";

            if ($user->isAdmin() == 0){
                echo "<form action='index.php?route=user&action=makeAdmin' method='post'>";
                echo "<input type='hidden' name='username' value='".$user->getUserName()."'>";
                echo "<td><input class='form-control btn btn-sm btn-danger'type='submit' value='Make Admin'/> </td>";
            }
            else {
                echo "<form action='index.php?route=user&action=makeUser' method='post'>";
                echo "<input type='hidden' name='username' value='".$user->getUserName()."'>";
                echo "<td><input class='form-control btn btn-sm btn-primary'type='submit' value='Make User'/> </td>";
            }
            echo "</form></tr>";
        }
        echo "</table>";
        echo "</div> </div>";
    }
}