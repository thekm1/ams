<?php
class View_Ams_View {
private $model;

	public function __construct(Model_Ams $model){
		$this->model = $model;
	}

	public function render(){
		echo "<table class='table table-sm table-hover'>";
		echo $this->renderTableHeader('AMS-Name','User','Host-Services','Status','Actions');
		if(isset($_SESSION['username']) && Controller_User::isAdmin()){
			$this->renderAms($this->model::getAllAms());//render all ams from database
		}
		else{
			$this->renderAms($this->model::getMyAms());// render all ams from current user
		}
		$this->getFormInRow($this->model);
		echo "</table>";
	}

	public function renderAms($amsArray){
		foreach ($amsArray as $ams){
			$this->renderRow($ams);
		}
	}

	private function renderRow(Model_Ams $ams){
		echo "<tr>";
		//name
		echo "<td>".$ams->getName()."</td>";
		//user
		echo "<td>".$ams->getUser()."</td>";
		//host and servie
		echo "<td>";
		echo "<table class='table table-sm table-bordered'>";
		echo $this->renderTableHeader('Host','Service');
		foreach ($ams->getHostServices() as $hostSer){
			echo "<tr>";
			echo "<td>".$hostSer['host']->getName()."</td>";
			echo "<td>".$hostSer['service']->getName()."</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "</td>";
		//status
		echo "<td>".$ams->getStatus()."</td>";
		//Actions
		echo "<form action='index.php?route=ams&action=delete' method='post'>";
		echo "<td>";
		echo "<input type='hidden' name='amsname' value='".$ams->getId()."'>";
		echo "<input class='form-control btn btn-sm btn-danger'type='submit' value='".t('del')."'/> ";
		echo "</td>";
		echo "</form>";
		echo "</tr>";
	}

	private function renderTableHeader(...$titles){
		$ret ="<thead><tr>";
		foreach($titles as $title){
			$ret .= "<th>$title</th>";
		}
		$ret .= "</tr></thead>";
		return $ret;
	}

	private function getFormInRow(Model_Ams $model) {
		echo "<script type='text/javascript' src='View/Ams/ams_form.js'></script>";
		echo "<tr>";
		echo "<form id='amsCreate' action='index.php?route=ams&action=create' method='post'>";
		echo "<td> <input autofocus id='AmsInputName'class='form-control form-control-sm' value='".$model->getName()."' type='text' name='name'> </td> ";//ams name 
		echo "<td> <input readonly id='AmsInputUsername'class='form-control form-control-sm' value='".$_SESSION['username']."' type='text' name='username'> </td> "; //user
		//host and servie
		echo "<td>";
		echo "<input id='AmsInputHostServices'class='form-control form-control-sm' value='' type='hidden' name='hostServices[]'>";//ams hostservices
		echo "<table id='selectedMetrics' class='table table-sm table-hover'>";
			echo $this->renderTableHeader('Host','Service','');
			//hostname selection
			echo "<td>";
			echo "<select class='form-control form-control-sm' name='hostname' id='hostnameInput'>";
					foreach(Model_Host::getAllHosts() as $host) {
						echo "<option>".$host->getName()."</option>";
					}
			echo "</select>";
			echo "</td>";
			//service name selection
			echo "<td>";
			echo "<select class='form-control form-control-sm' name='servicename' id='servicenameInput'>";
					foreach(Model_Service::getAllServices() as $service) {
						echo "<option>".$service->getName()."</option>";
					}
			echo "</select>";
			echo "</td>";
			//addbutton
			echo "<td>";
			echo "<button type='button' onclick='addSelection()' class='btn btn-sm btn-info form-control'>+</button>";
			echo "</td>";
		echo "</table>";
		//status
		echo "<td> <input readonly id='AmsInputStatus'class='form-control form-control-sm' value='".$model->getStatus()."' type='text' name='status'> </td> ";//ams status
		//action
		echo "<td> <input id='amsFormSubmitButton'class=' form-control btn btn-sm btn-primary'type='button' onclick='submitForm()' value='".t('create')."'/>";
		echo "</form>";
		echo "</tr>";
	}

}
