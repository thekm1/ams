function addSelection() {
  var host = $("#hostnameInput").find(":selected").text();
  var service = $("#servicenameInput").find(":selected").text();
  // console.log("The selected host is :"+host+" The selected service is :"+service);
  if(!inSelected(host,service)){
    $("#selectedMetrics").find("tbody").prepend("<tr class='value'><td>"+host+"</td><td>"+service+"</td><td><button type='button' class='remove btn btn-sm btn-danger form-control'>-</button></td></tr>");
    $("#selectedMetrics").find("tr.value").css({'background-color':''});
    $("#selectedMetrics tbody").removeClass('table-danger');
    $("#selectedMetrics").on('click','button.remove',function(event){
      $(this).parent().parent().remove();
    })
  }

}

function inSelected(host, service) {
  var found = false;
  $("#selectedMetrics").find("tbody>tr.value").each(function(){
    var hostfound = $(this).children('td:first').text();
    var servicefound = $(this).children('td:nth-child(2)').text();
    if(host === hostfound && service === servicefound){
      $(this).css({'background-color' :'red'});
      found = true;
      return;
    }
  });
  return found;
}

function submitForm(){
  var hostServices = getHostServices();
  var name = $("#AmsInputName").val();
  if(name === ''){
    $("#AmsInputName").attr({'placeholder':'Please write a Name'});
    $("#AmsInputName").css({'border-color':'red'});
    $("#AmsInputName").on('keyup',function(){
      if($(this).val != ''){
        $(this).css({'border-color':''});
      }
    });
  }
  else{
    $("#AmsInputName").css({'border-color':''});
  }
  if(hostServices.length < 1){
    $("#selectedMetrics tbody").addClass('table-danger');
  }

  if(name !== '' && hostServices.length >0){
    $("#AmsInputHostServices").val(JSON.stringify(hostServices));
    $("#amsCreate").submit();
  }
}

function getHostServices(){
  var hostservices = new Array();
  $("#selectedMetrics").find("tbody>tr.value").each(function(){
    var hostfound = $(this).children('td:first').text();
    var servicefound = $(this).children('td:nth-child(2)').text();
    hostservices.push({'host':hostfound,'service':servicefound});
  });
  return hostservices;
}