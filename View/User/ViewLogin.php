<?php
class View_User_ViewLogin {
private $model;

	public function __construct(Model_User $model){
		$this->model = $model;
	}

	public function render($error=''){
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2">';
        if($error !== ''){
            echo "<div class='alert alert-danger'><span>$error</span></div>";
        }
        if (isset($_SESSION["username"])){
            $user = Model_User::getUserByUsername($_SESSION["username"]);
            
                if (file_exists($_SERVER['DOCUMENT_ROOT'].'/AMS/img/'.$_SESSION["username"])){
                    echo '<p>';
                    echo '<img src="img/'.$_SESSION["username"].'" alt="user picture" width="100">';
                    echo '</p>';
                }
                echo '<form class="form-control" action="index.php?route=user&action=logoutUser" method="post">';
                echo '<label for="username" >Username</label>';
                echo '<br>';
                echo '<input class="form-control" id="username" type="text" name="username" value="'.$user['username'].'" readonly="readonly"><br>';
                echo '<button class="btn btn-primary" type="submit" value="Logoff">Logoff</button>';
                echo '</form>';    
                echo '<form class="form-control" action="index.php?route=user&action=updatePassword" method="post">';
                echo '<input class="form-control" id="username" type="hidden" name="username" value="'.$user['username'].'" readonly="readonly">';
                echo '<br>';
                echo '<p>Update Password<br>';
                echo '<input class="form-control" type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required><br>';
                echo '<input class="form-control" type="password" name="password2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>';
                echo '</p>';
                echo '<input class="btn btn-primary "type="submit" value="Update">';
            echo '</form>';
        }
        else{
        echo '
            <form class="form-control" action="index.php?route=user&action=loginUser" method="post">
                <p>Username
                <input class="form-control" type="text" name="username"><br>
                </p>
                <p>Password
                <input class="form-control" type="password" name="password"<br>
                </p>
                <input class="btn btn-primary" type="submit" value="Submit">
                <button class="btn btn-primary" type="submit" value="Register" formaction="index.php?route=user&action=renderRegistration">Register</button>
            </form>';
        }
        echo '</div>';
        echo '</div>';
    }
    public function renderLoginSuccessfull(){
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2 alert alert-success">';
        echo "<span>Password update was successfull</span>";
        echo '</div>';
        echo '</div>';
    }   
}