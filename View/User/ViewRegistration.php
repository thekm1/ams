<?php
class View_User_ViewRegistration {
private $model;

	public function __construct(Model_User $model){
		$this->model = $model;
	}

	public function render(){
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2">';
        echo '
        <div>
        <form class="form-control" action="index.php?route=user&action=create" method="post" enctype="multipart/form-data">
        <p>First name
        <input class="form-control" type="text" name="firstname" placeholder="Max" required>
        </p>
        <p>Last name
        <input class="form-control" type="text" name="lastname" placeholder="Muster" required>
        </p>
        <p>Username
        <input class="form-control" type="text" name="username" placeholder="Muster_88" required>
        </p>
        <p>Email
        <input class="form-control" type="email" name="email" placeholder="max.muster@domain.ch" required>
        </p>
        <p>Avatar
        <input class="form-control" type="file" name="picture" accept="image/*">
        </p>
        <p>Password
        <input class="form-control" type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
        title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" 
        placeholder="(0..9) & (a..z) & (A..Z) & (min 8 char)"required>
        </p>
        <p>Repeat password
        <input class="form-control" type="password" name="password2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
        title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  required>
        </p>
        <p>Accept AGBs?<br>
        <input class="form-control" type="checkbox" name="acceptagb" value="yes" title="you have to accept the AGBs" required> Yes<br>
        </p>
        <p>Finished?
        <input class="btn btn-primary"type="submit" text="submit">
        </p>
        </form>
        </div>';
        echo '</div>';
        echo '</div>';
	}
}