<?php
class View_User_ViewRegistrationSuccessfull {
//private $model;

	public function render($success, $errorarray){
        echo "<div class='row'>";
        echo '<div class="col-md-8 offset-md-2">';
        if ($success === true){
            echo '
            <div class="alert alert-success">
            <span>Registration Successfull</span>
            <span>You will recieve an registration email</span>
            </div>
            ';
        }

        else {
            echo "<div class='alert alert-danger'>";
            foreach ($errorarray as $error) {
                echo "-$error";
            }
            echo "<span>Please go back and remove error fields</span></div>";
        }
        echo '</div>';
        echo '</div>';
    }

}