$( document ).ready(function() {
	$("#createInputName").keyup(function() {
		var servicename = $("#createInputName").val();
		$.ajax({
			type: 'POST',
			url: "AjaxRouter.php?controller=Controller_Service&action=ajaxValidateName",
			data: {'servicename' : servicename },
			success: function(response){
				var resp = JSON.parse(response);
				if(resp.validation === true){
					$("#createInputName").css({'border-color': 'red'})
					$("input[value='Create']").prop('disabled', true);
					$("input[value='Create']").attr({'title':'Name allready exists'});
				}
				else{
					$("#createInputName").css({'border-color': 'green'})
					$("input[value='Create']").prop('disabled', false);
					$("input[value='Create']").attr({'title': null});
				}
			}
		})
	});
});