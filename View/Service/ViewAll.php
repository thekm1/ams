<?php
class View_Service_ViewAll {
private $model;

	public function __construct(Model_Service $model){
		$this->model = $model;
	}

	public function render(){
		echo "<script type='text/javascript' src='View/Service/ajaxValidation.js'></script>";
		echo "<table class='table table-sm table-hover'><tr><th>Servicename</th><th>Actions</th></tr>";
		foreach (Model_Service::getAllServices() as $service){
			echo "<tr>";
			echo "<td>".$service->getName()."</td>";
			echo "<form action='index.php?route=service&action=delete' method='post'>";
			echo "<input type='hidden' name='servicename' value='".$service->getName()."'>";
			echo "<td><input class='form-control btn btn-sm btn-danger'type='submit' value='".t('del')."'/> "."</td>";
			echo "</form></tr>";
		}
		echo "<tr>";
		echo "<form action='index.php?route=service&action=create' method='post'>";
		echo "<td> <input autofocus id='createInputName'class='form-control form-control-sm' type='text' name='servicename'> </td> ";
		echo "<td> <input class=' form-control btn btn-sm btn-primary'type='submit' value='".t('create')."'/>";
		echo "</tr>";
		echo "</table>";
	}
}