<?php 
function __autoload($class_name) {
	$filename = str_replace('_','/',$class_name).'.php';
	if ( ! file_exists($filename)){
		return false;
	}
	require_once($filename);
}