<?php 

//register your controller here in the router Array 
//groups that user can see without login
$router['home'] = ['controller'=>'Controller_Home','model'=> 'Model_Home', 'view' => 'View_Home_ViewHome'];
$router['user'] = ['controller'=>'Controller_User','model'=> 'Model_User', 'view' => 'View_User_ViewLogin'];
if(isset($_SESSION['username'])){//views that user can see with login
	$router['ams'] = ['controller'=>'Controller_Ams','model'=> 'Model_Ams', 'view' => 'View_Ams_View'];
}
if(isset($_SESSION['username']) && Controller_User::isAdmin($_SESSION['username'])){// views that user can see with admin rights
	$router['host'] = ['controller'=>'Controller_Host','model'=> 'Model_Host', 'view' => 'View_Host_ViewAll'];
	$router['service'] = ['controller'=>'Controller_Service','model'=> 'Model_Service', 'view' => 'View_Service_ViewAll'];
	$router['role'] = ['controller'=>'Controller_User','model'=> 'Model_User', 'view' => 'View_Role_ViewAll'];
}
if(array_key_exists($_GET['route'],$router)){	
	//use the parameters route to define the controller and action to define the action
	$model = new $router[$_GET['route']]['model']();
	$controller = new $router[$_GET['route']]['controller']($model);
	$view = new $router[$_GET['route']]['view']($model);
	
	if(isset($_GET['action'])) {
		$controller->{$_GET['action']}();
	}
	else{
		$view->render();
	}
}
else{
	if(in_array($_GET['route'],array('home','host','service','ams','role','user'))){
		View_Error::renderNoAccess();
	}
	else{
		View_Error::renderPageNotFound();
	}
}
